// source:study group, Elly ,jake

import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,

  };

  addItem = (event) => {
    if (event.key === "Enter") {
      const addTodo = {
        userId: 1,
        id: Math.floor(Math.random() * 20),
        title: event.target.value,
        completed: false
      }
      const newTodo = this.setState({
        todos: [...this.state.todos, addTodo]
      })
      event.target.value =""
    }
  }

  handleToggle = (event) => {
    let selectedItem = this.state.todos.map((todo)=> {
      if (event.target.id == todo.id) { 
       todo.completed = !todo.completed 
        console.log(todo)
      }
      return todo
    })
    console.log(selectedItem)
  this.setState(()=>({
    todos: selectedItem
  }))
  }

  deletedItem = (id) => {
    const changeItem = this.state.todos.filter(todos =>
      todos.id !== id);
      console.log(changeItem)
    this.setState({
      todos: changeItem
    })

  }
  
 handleDeleteComplete=()=>{
    console.log("works")
    const newTodos = this.state.todos.filter(
      todoItem => todoItem.completed  !== true
    )
    this.setState({ todos: newTodos })
  }


  render() {

    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" 
          onKeyDown={this.addItem} 
          placeholder="What needs to be done?" autoFocus />
        </header>
        <TodoList  
        todos={this.state.todos} 
        onCheck={this.handleToggle} 
        onChange={this.deletedItem} />
    
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button onClick={this.handleDeleteComplete}
         className="clear-completed">Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {


  render() {

    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input 
          className="toggle" 
          type="checkbox" 
          onChange={this.props.onCheck}
           checked={this.props.completed} 
           id={this.props.id} />
          <label>{this.props.title}</label>
          <button onClick={(event) => this.props.deletedItem(this.props.id)} className="destroy" />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem 
            
            title={todo.title} 
            onCheck={this.props.onCheck}
            completed={todo.completed} 
            id={todo.id} 
            deletedItem={this.props.onChange} />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;


